﻿﻿namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Aggregates.CreditApplicationAgg
{
    public enum AcquisitionChannel
    {
        Street = 1,
        
        Email =2,
        
        Sms = 3
    }

}
