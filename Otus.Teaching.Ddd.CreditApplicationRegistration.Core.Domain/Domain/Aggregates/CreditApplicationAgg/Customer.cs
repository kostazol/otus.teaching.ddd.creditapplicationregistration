﻿using System;
using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.SeedWork;

namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Aggregates.CreditApplicationAgg
{
    /// <summary>
    /// Customer
    /// </summary>
    public class Customer
        : DomainEntity
    {
        /// <summary>
        /// FullName of customer
        /// </summary>
        public FullName FullName { get; private set; }

        /// <summary>
        /// Contact of customer
        /// </summary>
        public Contact Contact { get; private set; }
        
        /// <summary>
        /// Канал привлечения (интернет-реклама, реклама на улице и т.д.)
        /// </summary>
        public AcquisitionChannel Channel { get; private set; }

        public Customer(FullName fullName, Contact contact, AcquisitionChannel channel)
        {
            Contact = contact ?? throw new ArgumentNullException(nameof(contact));
            FullName = fullName ?? throw new ArgumentNullException(nameof(fullName));
            Channel = channel;
        }

        protected Customer()
        {
            
        }
    }
}
