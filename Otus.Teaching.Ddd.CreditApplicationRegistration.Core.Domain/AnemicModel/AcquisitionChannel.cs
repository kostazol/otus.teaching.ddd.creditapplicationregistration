﻿﻿namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.AnemicModel
{
    public enum AcquisitionChannel
    {
        Street = 1,
        
        Email =2,
        
        Sms = 3
    }

}
