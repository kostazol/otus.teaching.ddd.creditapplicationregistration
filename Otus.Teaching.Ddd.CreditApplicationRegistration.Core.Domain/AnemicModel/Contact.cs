﻿﻿using System;

 namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.AnemicModel
{
    public class Contact
    {

        public Guid Id { get; set; }
        
        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Phone
        /// </summary>
        public string Phone { get; set; }
    }
}