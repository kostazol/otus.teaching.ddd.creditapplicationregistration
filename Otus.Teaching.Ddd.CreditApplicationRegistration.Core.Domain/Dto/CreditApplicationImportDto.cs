﻿using System;
using Otus.Teaching.Ddd.Contract;

namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Dto
{
    public class CreditApplicationImportDto
        : CreateNewCreditApplicationContract
    {
        public Guid Id { get; set; }
        
        public Guid CustomerId { get; set; }
        
        public string Channel { get; set; }
        
        public DateTime CreatedDate { get; set; }
        
        public string Status { get; set; }
        
        public decimal Sum { get; set; }
    }
}